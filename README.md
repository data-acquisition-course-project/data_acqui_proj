# Infant Temperature Monitoring System

Our project is based on an infant temperature monitoring system which includes the use of different **sensors** like temperature, motion, piezo buzzer, Zigbee, etc. The main idea behind our project is to monitor the temperature of the infant and notify the nurse or guardian when the temperature goes high. There will be two types of notifications one with LED blinking for temperature above normal range and a piezo buzzer which beeps if the temperature goes too high. We have implemented a prototype system that includes two temperature sensors for measuring the temperature of two different body parts of an infant.

This temperature will also get stored in a **database** which is not for project use but can be used for further analysis or study of infant temperature. We used Zigbees for establishing a **network** connection between infant and guardian or nurse room. We have also used a motion sensor in a baby room which will monitor the baby movement in order to start measuring the temperature. 

*This project is equally contributed by Yuhou Zhou, Dhanraj Negi, and Feven Legesse.*

Hardware Sketch:

![Sketch](./simulation/temp_bb.jpg)

Temperature visualization (The demo uses random data; the image can be reproduced by running *test/test_drawnnow.py*):

![Visulisation](./docs/Dec-07-2019 07-53-34.gif)

report: [Google Doc](https://docs.google.com/document/d/1B3WfP63gVpgd9iNCGeAcpvAWAkXw5pUwXpfrrIx15GE/edit)

