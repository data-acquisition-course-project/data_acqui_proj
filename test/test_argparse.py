import argparse

parser = argparse.ArgumentParser(description='Get Sender COM')
parser.add_argument('-c', '--com', type=str, help='the COM number of the sender Arduino')
args = parser.parse_args()

ArduinoCOM = 'COM' + args.com

print(ArduinoCOM)