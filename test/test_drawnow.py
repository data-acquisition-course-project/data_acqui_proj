import sqlalchemy as db
import datetime
import time
import matplotlib.pyplot as plt
from drawnow import *

# Generate fake data
import numpy as np
temp1_lst_full = np.random.uniform(25, 27, 1000)
temp2_lst_full = np.random.uniform(25, 28, 1000)


temp1_lst = []
temp2_lst = []
time_lst = []


def makeFig():  # Create a function that makes our desired plot
    WARN_EAR = 26
    WARN_ARM = 27
    plt.ylim(20, 30)  # Set y min and max values
    plt.title('Baby Temperature Monitor')  # Plot the title
    plt.grid(True)  # Turn the grid on
    plt.ylabel('Temp Ear')  # Set ylabels
    plt.plot(temp1_lst, 'ro-', label='Temp Ear')  # plot the temperature
    plt.axhline(WARN_EAR, c='tab:orange', linestyle='--', label='Ear Wanring')
    plt.legend(loc='upper left')  # plot the legend
    plt2 = plt.twinx()
    plt.ylim(20, 30)  # Create a second y axis
    plt2.plot(temp2_lst, 'b^-', label='Temp Arm')  # plot temp2_lst data
    plt2.set_ylabel('Temp Arm')
    plt2.axhline(WARN_ARM, linestyle='-.', label='Arm Warning')
    # Force matplotlib to NOT autoscale y axis
    plt2.ticklabel_format(useOffset=False)
    plt2.legend(loc='upper right')  # plot the legend


cnt = 0
while True:
    temp1_lst.append(temp1_lst_full[cnt])
    temp2_lst.append(temp2_lst_full[cnt])
    time_lst.append(datetime.datetime.now())
    # Draw
    drawnow(makeFig)
    # Pause Briefly. Important to keep drawnow from crashing
    plt.pause(.000001)
    cnt = cnt+1
    if(cnt > 10):  # If you have 50 or more points, delete the first one from the array
        temp1_lst.pop(0)  # This allows us to just see the last 50 data points
        temp2_lst.pop(0)
    time.sleep(1)
