import sqlalchemy as db
import serial
import datetime
import time
import matplotlib.pyplot as plt
from drawnow import *
import argparse

# Create a database engine
engine = db.create_engine('mysql://user10:user10@10.50.202.242/user10')
connection = engine.connect()
metadata = db.MetaData()

# Create a table objects
tmp_table = db.Table('temperature', metadata,
                     autoload=True, autoload_with=engine)

# Create a serial object
# Get COM number
parser = argparse.ArgumentParser(description='Get Sender COM')
parser.add_argument('-c', '--com', type=str, help='the COM number of the sender Arduino')
args = parser.parse_args()

ArduinoCOM = 'COM' + args.com
ser = serial.Serial(ArduinoCOM, 9600)

temp1_lst = []
temp2_lst = []
plt.ion()  # Tell matplotlib you want interactive mode to plot live data
cnt = 0


def makeFig():
    WARN_EAR = 26
    WARN_ARM = 29
    plt.ylim(20, 40)
    plt.title('Baby Temperature Monitor')
    plt.grid(True)
    plt.ylabel('Temp Ear')
    plt.plot(temp1_lst, 'ro-', label='Temp Ear')
    plt.axhline(WARN_EAR, c='tab:orange', linestyle='--', label='Ear Warning')
    plt.legend(loc='upper left')
    plt2 = plt.twinx()
    plt.ylim(20, 40)
    plt2.plot(temp2_lst, 'b^-', label='Temp Arm')
    plt2.set_ylabel('Temp Arm')
    plt2.axhline(WARN_ARM, linestyle='-.', label='Arm Warning')
    plt2.ticklabel_format(useOffset=False)
    plt2.legend(loc='upper right')


while True:
    receivestring = ser.readline().decode('utf-8').strip()
    print(receivestring)

    temp1, temp2 = receivestring.split(' ')
    temp1_lst.append(float(temp1))
    temp2_lst.append(float(temp2))

    # Draw
    drawnow(makeFig)
    # Pause Briefly. Important to keep drawnow from crashing
    plt.pause(.000001)
    cnt = cnt + 1
    if (cnt > 10):  # If you have 50 or more points, delete the first one from the array
        temp1_lst.pop(0)  # This allows us to just see the last 50 data points
        temp2_lst.pop(0)

    # Get current date and time
    now = datetime.datetime.now()
    dt_string = now.strftime("%Y/%m/%d %H:%M:%S").split()
    date = dt_string[0]
    t = dt_string[1]

    # Insert data to the database
    query = db.insert(tmp_table).values(
        did=1, date=date, time=t, temperature=temp1)
    ResultProxy = connection.execute(query)
    query = db.insert(tmp_table).values(
        did=2, date=date, time=t, temperature=temp1)
    ResultProxy = connection.execute(query)

    time.sleep(1)
