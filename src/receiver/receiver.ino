#include <SoftwareSerial.h>
char state;
short xBeeTx = 11;
short xBeeRx = 12;
int PIEZO = 5;
int LED = 13;
int delay_time_unit = 250;

SoftwareSerial xBeeSerial(xBeeRx, xBeeTx);

void setup()
{
  xBeeSerial.begin(9600);
  Serial.begin(9600);
  pinMode(PIEZO, OUTPUT);
}

void loop()
{
  if (xBeeSerial.available() > 0)
  {
    state = xBeeSerial.read();
    Serial.println(state);
    if (state == 'H')
    {
      digitalWrite(PIEZO, HIGH);
      delay(delay_time_unit);
    }
    else if (state == 'L')
    {
      digitalWrite(PIEZO, LOW);
      delay(delay_time_unit);
    }
    else if (state == 'M')
    {
        digitalWrite(LED, HIGH);
        delay(delay_time_unit);
        digitalWrite(LED, LOW);
        delay(delay_time_unit);
    }
    else
    {
      digitalWrite(LED, LOW);
      delay(delay_time_unit);
    }
  }
}
