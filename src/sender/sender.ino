#include <SoftwareSerial.h>
#define NR_SAMPLES 10
#define WARN_EAR 26
#define WARN_ARM 29
short xBeeTx = 11;
short xBeeRx = 12;
int TMP1 = A0; // The middle lead is connected to analog - in 0.
int TMP2 = A3;
int LED = 13;
int PIR = 7;
int motion_status = 0;
int temperature = 0;
int wait_ms = 20; // wait time between measurements in millisec.
int samples[NR_SAMPLES]; // array of samples
int delay_time_unit = 250;


SoftwareSerial xBeeSerial(xBeeRx, xBeeTx);

void setup()
{
    Serial.begin(9600);
    xBeeSerial.begin(9600);
    pinMode(LED, OUTPUT);
    pinMode(PIR, INPUT);
}

float cal_temp(int tempPin)
{
    float sum = 0.0;
    for (int i = 0; i < NR_SAMPLES; ++i)
    {
        // map values from range [0 , 410] to [ -50 , 150]
        samples[i] = map(analogRead(tempPin), 0, 410, -50, 150);
        sum += samples[i];
        delay(wait_ms);
    }
    float mean = sum / NR_SAMPLES;
    return mean;
}

void loop()
{
    float tmp1 = cal_temp(TMP1);
    float tmp2 = cal_temp(TMP2);
    Serial.print(tmp1);
    Serial.print(' ');
    Serial.println(tmp2);
    motion_status = digitalRead(PIR);

    if (tmp1 > WARN_EAR && tmp2 > WARN_ARM)
    {
        digitalWrite(LED, HIGH);
        delay(delay_time_unit);
        digitalWrite(LED, LOW);
        delay(delay_time_unit);
    }

    if (tmp1 > (WARN_EAR + 1) && tmp2 > (WARN_ARM + 1))
    {
        xBeeSerial.print('H');
        delay(2 * delay_time_unit);
    }
    else
    {
        xBeeSerial.print('L');
        delay(2 * delay_time_unit);
    }
    
    if (motion_status == HIGH)
    {
        xBeeSerial.print('M');
        delay(2 * delay_time_unit);
    }
    else
    {
        xBeeSerial.print('S');
        delay(2 * delay_time_unit);
    }
}
